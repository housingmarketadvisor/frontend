import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { CoreComponent } from './core.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    CoreComponent,
    WelcomeComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    NgbCarouselModule
  ]
})
export class CoreModule { }
