import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './share/layout/main-layout/main-layout.component';
import { EmployeeLayoutComponent } from './share/layout/employee-layout/employee-layout.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./core/core.module').then((m) => m.CoreModule),
    component: MainLayoutComponent,
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
    component: MainLayoutComponent,
  },
  {
    path: 'offers',
    loadChildren: () =>
      import('./offers/offers.module').then((m) => m.OffersModule),
    component: MainLayoutComponent,
  },
  {
    path: 'currency',
    loadChildren: () =>
      import('./currency/currency.module').then((m) => m.CurrencyModule),
    component: MainLayoutComponent,
  },
  {
    path: 'employee',
    loadChildren: () =>
      import('./employee/employee.module').then((m) => m.EmployeeModule),
    component: EmployeeLayoutComponent,
  },
  {
    // wildcard route
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
