import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CurrencyService} from "../services/currency.service";
import {CurrencyModel} from "../models/currency.model";

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.less'],
})
export class CurrencyComponent implements OnInit {

  public currency: CurrencyModel[] = [];
  activePage: number = 0;

  constructor(public currencyService: CurrencyService) { }

  ngOnInit(): void {
    this.currencyService.getCurrency().subscribe((currency) => {
      this.currency = currency;
    });
  }

  public goToPreviousPage(): void {
    if (this.activePage > 0) {
      this.activePage = this.activePage - 1;
    }
  }

  public getTotalPages(): number[] {
    return [...Array(Math.ceil(this.currency.length / 10)).keys()];
  }

  public goToNextPage(): void {
    if (this.activePage < this.currency.length - 1) {
      this.activePage = this.activePage + 1;
    }
  }

  public changePage(page: number): void {
    this.activePage = page;
  }
}
