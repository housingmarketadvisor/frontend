import { Injectable } from '@angular/core';
import {CurrencyModel} from "../models/currency.model";
import {Observable, of} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ApiService} from "../../share/services/api.service";


@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor(private http: HttpClient, private configApi: ApiService) { }

  public getCurrency(): Observable<CurrencyModel[]> {
    return this.http.get<CurrencyModel[]>(this.configApi.getApiUrl() + '/CurrencyExchangeRate');
  }
}
