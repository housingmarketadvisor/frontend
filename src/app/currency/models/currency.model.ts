export interface CurrencyModel {
  id:             number;
  date:           Date;
  rate:           number;
  baseCurrency:   string;
  targetCurrency: string;
}
