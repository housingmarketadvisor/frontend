import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CurrencyRoutingModule } from './currency-routing.module';
import { CurrencyComponent } from './components/currency.component';
import { CurrencyFiltersComponent } from './components/currency-filters/currency-filters.component';


@NgModule({
  declarations: [
    CurrencyComponent,
    CurrencyFiltersComponent
  ],
  imports: [
    CommonModule,
    CurrencyRoutingModule
  ]
})
export class CurrencyModule { }
