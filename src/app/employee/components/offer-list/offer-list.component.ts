import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {BehaviorSubject, Observable, of, tap} from 'rxjs';
import { Offer } from 'src/app/offers/models/offers.model';
import { ListService } from 'src/app/offers/services/list.service';
import {FilterType} from "../../../offers/components/list/list-filters/list-filters.component";

@Component({
  selector: 'app-offer-list',
  templateUrl: './offer-list.component.html',
  styleUrls: ['./offer-list.component.less'],
})
export class OfferListComponent implements OnInit {
  public offers$: Observable<Offer[]> = of([]);
  activePage: number = 0;
  offersLength = 0;
  private filters$: BehaviorSubject<Partial<FilterType> | {}> = new BehaviorSubject<Partial<FilterType> | {}>({});
  constructor(public offerService: ListService, private router: Router) {}

  public ngOnInit(): void {
    this.fetchOffers();
  }

  public delete(offer: Offer): void {
     if(window.confirm('Czy chcesz usunąć ofertę?')) {
      this.offerService.deleteOffer(offer.id).subscribe(() => {
        this.offers$ = this.offerService.fetchOffers();
      });
     }
  }

  public edit(offer: Offer): void {
    this.router.navigate(['employee', 'add'], {state: {id: offer.id}})
  }

  public getTotalPages(): number[] {
    return [...Array(Math.ceil(this.offersLength / 15)).keys()];
  }

  public goToPreviousPage(): void {
    if (this.activePage > 0) {
      this.activePage = this.activePage - 1;
    }
  }

  public goToNextPage(): void {
    if (this.activePage < this.offersLength - 1) {
      this.activePage = this.activePage + 1;
    }
  }

  changePage(page: number) {
    this.activePage = page;
  }

  applyFilters($event: Partial<FilterType>) {
    this.filters$.next($event);
    this.fetchOffers();
  }

  private fetchOffers() {
    this.offers$ = this.offerService.fetchOffers(this.filters$.getValue()).pipe(
      tap((offers) => {
        this.offersLength = offers.length;
      })
    );
  }
}
