import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators,} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {filter, Subject, takeUntil} from 'rxjs';
import {ListService} from 'src/app/offers/services/list.service';
import {Offer} from 'src/app/offers/models/offers.model';

interface FormType {
  title: FormControl<string>;
  description: FormControl<string>;
  price: FormControl<number>;
  address: FormControl<string>;
  city: FormControl<string>;
  zipCode: FormControl<string>;
  country: FormControl<string>;
  contact: FormControl<string>;
  phone: FormControl<string>;
  email: FormControl<string>;
  website: FormControl<string>;
  imageUrl: FormControl<string>;
  imageUrl2: FormControl<string>;
  imageUrl3: FormControl<string>;
}


@Component({
  selector: 'app-add-offer',
  templateUrl: './add-offer.component.html',
  styleUrls: ['./add-offer.component.less'],
})
export class AddOfferComponent implements OnInit, OnDestroy {
  public form: FormGroup<FormType> | null = null;

  private offerId: number | null = null;
  private offer: Offer | null = null;
  private ngOnDestroy$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private listService: ListService
  ) {
    this.checkIsEditing();
  }

  public isEditing(): boolean {
    return !!this.offerId;
  }

  ngOnInit(): void {
    this.initForm();
    this.startEditMode();
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
    this.ngOnDestroy$.complete();
  }

  public getFormControlName(key: keyof FormType): string {
    return key;
  }

  submitForm() {
    if (this.isEditing()) {
      this.editOffer();
    } else {
      this.createOffer();
    }
  }

  private checkIsEditing(): void {
    const {id} = this.router.getCurrentNavigation()?.extras.state! || {};
    this.offerId = id;
  }

  private initForm(): void {
    this.form = this.fb.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      price: [0, [Validators.required]],
      address: ['', [Validators.required]],
      city: ['', [Validators.required]],
      zipCode: ['', [Validators.required]],
      country: ['', [Validators.required]],
      contact: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      email: ['', [Validators.required]],
      website: ['', [Validators.required]],
      imageUrl: ['', [Validators.required]],
      imageUrl2: ['', []],
      imageUrl3: ['', []],
    }) as FormGroup<FormType>;
  }

  private startEditMode(): void {
    if (this.offerId) {
      this.listService
        .getOffer(this.offerId)
        .pipe(
          filter((offer) => !!offer),
          takeUntil(this.ngOnDestroy$)
        )
        .subscribe((offer) => {
          this.offer = offer;
          this.patchForm(offer);
        });
    }
  }

  private patchForm(offer: Offer): void {
    this.form?.patchValue({
      title: offer.title,
      description: offer.description,
      price: offer.price,
      address: offer.address,
      city: offer.city,
      zipCode: offer.zipCode,
      country: offer.country,
      contact: offer.contact,
      phone: offer.phone,
      email: offer.email,
      website: offer.website,
      imageUrl: offer.imageUrl,
      imageUrl2: offer.imageUrl2,
      imageUrl3: offer.imageUrl3,
    });
  }

  private createOffer(): void {
    this.listService.createOffer({
      ...this.form?.getRawValue() as Offer,
      imageUrl2: this.form?.get('imageUrl2')!.value!,
      imageUrl3: this.form?.get('imageUrl')!.value!
    }).subscribe(() => {
      console.log('created');
      this.router.navigate(['/employee/offers']);
    });
  }

  private editOffer(): void {
    this.listService.editOffer({...this.form?.getRawValue() as Offer, id: this.offer?.id!}).subscribe(() => {
      console.log('edited');
      this.router.navigate(['/employee/offers']);
    });
  }
}
