import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeComponent } from './employee.component';
import { AddOfferComponent } from './components/add-offer/add-offer.component';
import { OfferListComponent } from './components/offer-list/offer-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import {OffersModule} from "../offers/offers.module";


@NgModule({
  declarations: [
    EmployeeComponent,
    AddOfferComponent,
    OfferListComponent
  ],
    imports: [
        CommonModule,
        EmployeeRoutingModule,
        ReactiveFormsModule,
        OffersModule,
    ]
})
export class EmployeeModule { }
