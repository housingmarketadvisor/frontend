import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";

type LoginForm = {
  email: FormControl<string | null>;
  password: FormControl<string | null>;
}

const email = 'email@test_email.pl';
const password = 'password';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  form: FormGroup<LoginForm> | null = null;

  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  public onLogin() {
    this.checkCredentials() ? this.router.navigate(['employee', 'offer-list']) : alert('Niepoprawne dane logowania');
  }

  private initForm(): void {
    this.form = this.fb.group({
      email: [email],
      password: [password]
    });
  }

  private checkCredentials(): boolean {
    return this.form?.value.email === email && this.form?.value.password === password;
  }
}
