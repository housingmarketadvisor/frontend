import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {ShareModule} from "./share/share.module";
import {HttpClientModule} from "@angular/common/http";

import {APP_INITIALIZER} from "@angular/core";
import {ApiService} from "./share/services/api.service";

export function initializeApp(appInitializerService: ApiService) {
  return (): Promise<any> => {
    return appInitializerService.loadApiURL();
  }
}


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ShareModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [ApiService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
