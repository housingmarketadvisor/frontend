import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

type Configuration = {
  apiUrl: string;
  currencyApi: string;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private readonly CONFIG_API_URL = environment.production ?
    './config/configuration.json' :
    '../../../assets/config/configuration.json';

  private configApi: Configuration | null = null;

  constructor(private http: HttpClient) { }

  public getApiUrl(): string {
    return this.configApi!.apiUrl;
  }

  public getCurrencyApiUrl(): string {
    return this.configApi!.currencyApi;
  }

  public loadApiURL(): Promise<any> {
    return this.http.get(this.CONFIG_API_URL)
      .toPromise()
      .then(config => {
        console.log(config);
        this.configApi = config as Configuration;
      })
      .catch(err => console.error(err));
  }

}
