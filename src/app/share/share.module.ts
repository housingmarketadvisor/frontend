import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShareRoutingModule } from './share-routing.module';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import {RouterModule} from "@angular/router";
import { NavbarComponent } from './layout/main-layout/navbar/navbar.component';
import { FooterComponent } from './layout/main-layout/footer/footer.component';
import { EmployeeLayoutComponent } from './layout/employee-layout/employee-layout.component';


@NgModule({
  declarations: [
    MainLayoutComponent,
    NavbarComponent,
    FooterComponent,
    EmployeeLayoutComponent
  ],
  imports: [
    CommonModule,
    ShareRoutingModule,
    RouterModule
  ], exports: [ MainLayoutComponent, EmployeeLayoutComponent ]
})
export class ShareModule { }
