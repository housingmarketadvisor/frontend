import {Injectable} from '@angular/core';
import {catchError, Observable, of} from "rxjs";
import {Offer} from "../models/offers.model";
import {HttpClient} from "@angular/common/http";
import {apiURL} from "../../../environments/environment";
import {ApiService} from "../../share/services/api.service";
import {Params} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class ListService {

  constructor(private http: HttpClient, private apiUrl: ApiService) {
  }

  public fetchOffers(params: Params = {}): Observable<Offer[]> {
    return this.http.get<Offer[]>(`${this.apiUrl.getApiUrl()}/HousingOffer`, {params: params})
  }

  public getOffer(id: number): Observable<Offer> {
    return this.http.get<Offer>(`${this.apiUrl.getApiUrl()}/HousingOffer/${id}` )
  }

  public createOffer(offer: Offer): Observable<Offer> {
    return this.http.post<Offer>(`${this.apiUrl.getApiUrl()}/HousingOffer`, offer)
  }

  public editOffer(offer: Offer): Observable<Offer> {
    return this.http.put<Offer>(`${this.apiUrl.getApiUrl()}/HousingOffer/${offer.id}`, offer)
  }

  public deleteOffer(id: number): Observable<Offer> {
    return this.http.delete<Offer>(`${this.apiUrl.getApiUrl()}/HousingOffer/${id}`)
  }
}
