import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListComponent} from "./components/list/list.component";
import {OfferDescriptionComponent} from "./components/offer-description/offer-description.component";

const routes: Routes = [
  {path: 'list', component: ListComponent},
  {path:':id' ,component: OfferDescriptionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffersRoutingModule { }
