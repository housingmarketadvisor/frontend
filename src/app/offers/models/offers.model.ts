export interface Offer {
  price: number;
  id: number;
  title: string;
  description: string;
  address: string;
  city: string;
  zipCode: string;
  country: string;
  contact: string;
  phone: string;
  email: string;
  website: string;
  imageUrl: string;
  imageUrl2: string;
  imageUrl3: string;
}
