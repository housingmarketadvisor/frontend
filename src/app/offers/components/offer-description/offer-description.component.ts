import { Component, OnInit } from '@angular/core';
import {ListService} from "../../services/list.service";
import {ActivatedRoute} from "@angular/router";
import {Observable, of} from "rxjs";
import {Offer} from "../../models/offers.model";

@Component({
  selector: 'app-offer-description',
  templateUrl: './offer-description.component.html',
  styleUrls: ['./offer-description.component.less']
})
export class OfferDescriptionComponent implements OnInit {

  offer$: Observable<Offer | null> = of(null)

  constructor(private listService: ListService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.offer$ = this.listService.getOffer(params["id"]!);
    })
  }

}
