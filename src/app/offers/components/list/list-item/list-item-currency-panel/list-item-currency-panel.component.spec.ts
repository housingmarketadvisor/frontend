import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemCurrencyPanelComponent } from './list-item-currency-panel.component';

describe('ListItemCurrencyPanelComponent', () => {
  let component: ListItemCurrencyPanelComponent;
  let fixture: ComponentFixture<ListItemCurrencyPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListItemCurrencyPanelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListItemCurrencyPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
