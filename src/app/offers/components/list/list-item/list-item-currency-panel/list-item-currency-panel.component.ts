import {Component, Input, OnInit} from '@angular/core';
import {CurrencyService} from "../../../../../currency/services/currency.service";
import {Offer} from "../../../../models/offers.model";
import {CurrencyModel} from "../../../../../currency/models/currency.model";

type CurrencyConverted = Pick<CurrencyModel, 'baseCurrency' | 'rate' | 'targetCurrency'> & { converted: number | string}

@Component({
  selector: 'app-list-item-currency-panel',
  templateUrl: './list-item-currency-panel.component.html',
  styleUrls: ['./list-item-currency-panel.component.less']
})
export class ListItemCurrencyPanelComponent implements OnInit {

  @Input()
  offer: Offer | null = null;

  public currencies: CurrencyConverted[] = [];

  constructor(private currencyService: CurrencyService) { }


  ngOnInit(): void {
    this.currencyService.getCurrency().subscribe((data) => {
      this.currencies = data.map((item) => ({
        baseCurrency: item.baseCurrency,
        rate: item.rate,
        targetCurrency: item.targetCurrency,
        converted: (this.offer?.price! / item.rate ).toFixed(2),
      } as CurrencyConverted))
    });
  }

}
