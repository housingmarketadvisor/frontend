import {Component, Input, OnInit} from '@angular/core';
import {Offer} from "../../../models/offers.model";

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.less']
})
export class ListItemComponent implements OnInit {
  @Input()
  offer: Offer | null = null;

  constructor() { }

  ngOnInit(): void {
  }

}
