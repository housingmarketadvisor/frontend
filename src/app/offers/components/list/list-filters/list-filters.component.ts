import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Offer } from 'src/app/offers/models/offers.model';

export interface FilterType {
  title: string;
  minPrice: number;
  maxPrice: number;
  address: string;
  city: string;
  zipCode: string;
  country: string;
}

@Component({
  selector: 'app-list-filters',
  templateUrl: './list-filters.component.html',
  styleUrls: ['./list-filters.component.less']
})
export class ListFiltersComponent implements OnInit {
  public form: FormGroup | null = null;

  @Output()
  public onFiltersChange = new EventEmitter<Partial<FilterType>>();

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
  }

  public getFormControlByKey(key: keyof FilterType): string {
    return key;
  }

  public submitForm(): void {
    const e: Partial<FilterType> = {};

    Object.entries(this.form?.getRawValue()).forEach(([key, value]) => {
      if(value) {
        // @ts-ignore
        e[key] = value;
      }
    });

    this.onFiltersChange.next(e)
  }

  private initForm(): void {
    this.form = this.fb.group({
      'title': [''],
      'minPrice': [''],
      'maxPrice': [''],
      'address': [''],
      'city': [''],
      'zipCode': [''],
      'country': [''],
    });
  }

  clearFilters() {
    this.form?.reset();
    this.submitForm();
  }
}
