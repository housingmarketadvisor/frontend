import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ListService} from "../../services/list.service";
import {BehaviorSubject, Observable, of, tap} from "rxjs";
import {Offer} from "../../models/offers.model";
import {FilterType} from "./list-filters/list-filters.component";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent implements OnInit {
  public offers$: Observable<Offer[]> = of([])
  public activePage: number = 0;

  public offersLength: number = 0;
  private filters$: BehaviorSubject<Partial<FilterType> | {}> = new BehaviorSubject<Partial<FilterType> | {}>({});

  constructor(private listService: ListService) {
  }

  ngOnInit(): void {
    this.fetchOffers();
  }

  public getTotalPages(): number[] {
    return [...Array(Math.ceil(this.offersLength / 10)).keys()];
  }

  public applyFilters($event: Partial<FilterType>) {
    this.filters$.next($event);
    this.fetchOffers();
  }

  public changePage(page: number): void {
    this.activePage = page;
  }

  public goToPreviousPage() {
    if (this.activePage > 0) {
      this.activePage = this.activePage - 1;
    }
  }

  public goToNextPage() {
    if (this.activePage < this.getTotalPages().length - 1) {
      this.activePage = this.activePage + 1;
    }
  }

  private fetchOffers(): void {
    this.offers$ = this.listService.fetchOffers(this.filters$.getValue())
      .pipe(
        tap((offers) => {
          this.offersLength = offers.length
        })
      )
  }
}
