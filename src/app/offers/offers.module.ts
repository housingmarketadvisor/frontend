import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OffersRoutingModule } from './offers-routing.module';
import { ListComponent } from './components/list/list.component';
import { ListItemComponent } from './components/list/list-item/list-item.component';
import { ListFiltersComponent } from './components/list/list-filters/list-filters.component';
import { OfferDescriptionComponent } from './components/offer-description/offer-description.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ListItemCurrencyPanelComponent } from './components/list/list-item/list-item-currency-panel/list-item-currency-panel.component';

@NgModule({
    declarations: [
        ListComponent,
        ListItemComponent,
        ListFiltersComponent,
        OfferDescriptionComponent,
        ListItemCurrencyPanelComponent
    ],
    exports: [
        ListFiltersComponent
    ],
    imports: [
        CommonModule,
        OffersRoutingModule,
        ReactiveFormsModule,
    ]
})
export class OffersModule { }
